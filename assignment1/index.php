<?php

    if(!empty($_POST)){
      require_once ('QueryHelper.php');
      $dlength = $_POST['dictionary_length'];
      $darray = $_POST['dic_value'];
      $wlength = $_POST['word_length'];
      $qlength = $_POST['query_length'];
      $qarray = $_POST['query_value'];
      $search_object = new QueryHelper($dlength, $darray, $wlength, $qlength, $qarray);
      $output_array = $search_object->renderOutput();
    }

?>


<!DOCTYPE html>
<html lang="en">
<head>
  <title>Assignment 1</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
  <script src="js/ass1.js"></script>
</head>
<body>
  <div class="container">
      <div class="row col-sm-12">
        <form action="" method="post" name="assignment1" >
          <h1>Assignment 1 form</h1>
          <div class="form-group">
            <label for="dictionary_length">Enter Dictionary Length</label>
            <input type="text" class="form-control" id="dictionary_length" name="dictionary_length">
          </div>
          <div class="form-group">
            <label for="word_length">Enter Word Length</label>
            <input type="text" class="form-control" id="word_length" name="word_length">
          </div>
            <div class="form-group" id="dictionary_wrapper">

            </div>
          <div class="form-group">
              <label for="dictionary_length">Enter number of queries</label>
              <input type="text" class="form-control" id="query_length" name="query_length">
          </div>
            <div class="form-group" id="query_wrapper">

            </div>
          <button type="submit" class="btn btn-primary">Submit</button>
        </form>
      </div>
    <div class="row col-sm-12">
    <?php
    if(!empty($output_array)){ ?>
      <table class="table table-striped">
        <thead>
          <tr>
            <th>Query</th>
            <th>Count</th>
          </tr>
        </thead>
          <tbody>
          <?php
            foreach($output_array as $key => $value){
                print '<tr><td>'.$qarray[$key].'</td><td>'.$value.'</td></tr>';
            }
          ?>
          </tbody>
      </table>
    <?php }
    ?>
    </div>
  </div>
</body>
</html>
