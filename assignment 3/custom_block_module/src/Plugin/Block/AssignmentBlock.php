<?php
namespace Drupal\custom_block_module\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\file\Entity\File;

/**
 * Provides a 'CustomBlock' block.
 *
 * @Block(
 *  id = "assignment_block",
 *  admin_label = @Translation("Assignment Block"),
 * )
 */
class AssignmentBlock extends BlockBase implements BlockPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [

        'image' => [
          'value' => ''
        ],

        'label_display' => FALSE,
      ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);
    $config = $this->getConfiguration();
    $form['title'] = array (
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('Title'),
      '#default_value' => isset($config['title']) ? $config['title'] : '',

    );
    $form['description'] = array (
      '#type' => 'text_format',
      '#required' => TRUE,
      '#title' => $this->t('Description'),
      '#description' => $this->t('Enter Description'),
      '#default_value' => isset($config['description']) ? $config['description'] : '',
    );
    $form['image'] = array(
      '#type' => 'managed_file',
      '#required' => TRUE,
      '#upload_location' => 'public://images/',
      '#title' => $this->t('Image'),
      '#description' => t("Image to show in the block"),
      '#default_value' => $this->configuration['image'],
      '#upload_validators' => array(
        'file_validate_extensions' => array('gif png jpg jpeg'),
        'file_validate_size' => array(25600000),
      ),
      '#states'        => array(
        'visible'      => array(
          ':input[name="image_type"]' => array('value' => t('Upload New Image(s)')),
        )
      )
    );
    $form['restrict_display'] = array(
      '#type'          => 'checkbox',
      '#title'         => t('Restrict By IP Address'),
      '#default_value' => isset($config['restrict_display']) ? $config['restrict_display'] : '',
    );
    $form['ip_addresses'] = array(
      '#type'          => 'textarea',
      '#title'         => t('IP Addresses'),
      '#default_value' => isset($config['ip_addresses']) ? $config['ip_addresses'] : '',
      '#description' => t('Enter IP Address comma separated'),
    );
    return $form;
  }
  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->setConfigurationValue('title', $form_state->getValue('title'));
    $this->setConfigurationValue('description', $form_state->getValue('description'));
    $this->setConfigurationValue('restrict_display', $form_state->getValue('restrict_display'));
    $this->setConfigurationValue('ip_addresses', $form_state->getValue('ip_addresses'));
    $image = $form_state->getValue('image');
    $this->configuration['image'] = $image;
    $file = File::load( $image[0] );
    $file->setPermanent();
    $file->save();
  }
  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = $this->getConfiguration();
    $build = array();
    if (isset($config['image']) && !empty($config['image'])) {
      $image_field = $config['image'];
      $image_uri = File::load($image_field[0]);
      $build['image'] = [
        '#theme' => 'image_style',
        '#style_name' => 'medium',
        '#uri' => $image_uri->uri->value
      ];
    } else {
      $build['image']['#markup'] = '['.t('Picture').']';
    }
    $build['title']['#markup'] = $config['title'];
    $build['description']['#markup'] = $config['description']['value'];
    return $build;
  }
}