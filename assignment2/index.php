<?php

/**
 * @file
 */

    if (!empty($_POST)) {
      require_once 'HackNumber.php';

      $testcase = 3;

      $s1array = $_POST['string1'];
      $s2array = $_POST['string2'];
      $carray = $_POST['c'];
      $iarray = $_POST['i'];


      $search_object = new HackNumber($s1array, $s2array, $iarray, $carray, $testcase);

      $output_array = $search_object->renderOutput();
    }

?>


<!DOCTYPE html>
<html lang="en">
<head>
  <title>Assignment 1</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
  <script src="js/ass2.js"></script>
</head>
<body>
  <div class="container">
  <div class="row col-sm-12">
    <form action="" method="post" name="assignment1" >
      <h1>Assignment 2 form</h1>
      <div class="form-group">
        <label for="dictionary_length">Number of test cases</label>
        <input type="text" class="form-control" id="test_cases" name="test_cases">
      </div>
        <div class="form-group" id="testcase_wrapper">

        </div>
      <button type="submit" class="btn btn-primary">Submit</button>
    </form>
  </div>
    <div class="row col-sm-12">
    <?php
    if (!empty($output_array)) { ?>
      <table class="table table-striped">
        <thead>
          <tr>
            <th>Query</th>
            <th>Count</th>
          </tr>
        </thead>
          <tbody>
          <?php
            foreach ($output_array as $key => $value) {
            print '<tr><td>Test Case' . ($key+1) . '</td><td>' . $value . '</td></tr>';
            }
          ?>
          </tbody>
      </table>
    <?php }
    ?>
    </div>
  </div>
</body>
</html>
