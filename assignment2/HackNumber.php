<?php

class HackNumber {
  private $s1array;
  private $s2array;
  private $iarray;
  private $carray;
  private $testcase;
  private $resultarray;

  /**
   * HackNumber constructor.
   *
   * @param $s1array
   * @param $s2array
   * @param $iarray
   * @param $carray
   * @param $testcase
   */
  public function __construct ($s1array, $s2array, $iarray, $carray, $testcase) {
    $this->s1array = $s1array;
    $this->s2array = $s2array;
    $this->iarray = $iarray;
    $this->testcase = $testcase;
    $this->carray = $carray;
    $this->resultarray = array();
  }

  /**
   *
   */
  private function generateOutput() {
    for($i=0; $i < $this->testcase; $i++) {
      $this->isHackNumber($this->s1array[$i],$this->s2array[$i],$this->iarray[$i],$this->carray[$i]);
    }
  }

  /**
   * @param $query_pattern
   */
  private function isHackNumber($string1, $string2, $i, $c) {

    if($c=='Y'){
      $search_key = ' '.$string2.' ';
    }else{
      $search_key = $string2;
    }

    $position = strpos($string1, $search_key, $i);
    if($position !== FALSE){
      if($c=='Y'){
        $position = $position+1;
      }
    }else{
      $position = 'No worries';
    }

    $this->resultarray[] = $position;
  }


  /**
   * @return array
   */
  public function renderOutput(){
    $this->generateOutput();
    return $this->resultarray;
  }
}
