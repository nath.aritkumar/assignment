(function($, window, document) {
  $(function() {
    $('#dictionary_length').on('blur',function () {
      var dictionaryLength = 0;
      dictionaryLength = $('#dictionary_length').val();
      $( "#dictionary_wrapper" ).empty();
      if(dictionaryLength && $.isNumeric( dictionaryLength ) && dictionaryLength > 0) {
        $('#dictionary_wrapper').append('<label for="word_length">Dictionary</label>');
        for(var i=0; i< dictionaryLength; i++){
          $('#dictionary_wrapper').append('<input type="text" class="form-control" name="dic_value[]">')
        }
      }
    });
    $('#query_length').on('blur',function () {
      var queryLength = 0;
      queryLength = $('#query_length').val();
      $( "#query_length" ).empty();
      if(queryLength && $.isNumeric( queryLength ) && queryLength > 0) {
        $('#query_wrapper').append('<label for="word_length">Queries</label>');
        for(var i=0; i< queryLength; i++){
          $('#query_wrapper').append('<input type="text" class="form-control" name="query_value[]">')
        }
      }
    });
  });
})(jQuery);
