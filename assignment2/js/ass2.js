(function($, window, document) {
  $(function() {
    $('#test_cases').on('blur',function () {
      var test_cases = 0;
      test_cases = $('#test_cases').val();
      $( "#testcase_wrapper" ).empty();
      if(test_cases && $.isNumeric( test_cases ) && test_cases > 0) {
        //$('#testcase_wrapper').append('');
        for(var i=0; i< test_cases; i++){
          $counter = i+1;
          $('#testcase_wrapper').append('<h6>Test Case '+$counter+'</h6>\n' +
              '            <div class="form-group">\n' +
              '                <label for="string1">String 1</label>\n' +
              '                <input type="text" class="form-control" name="string1[]">\n' +
              '            </div>\n' +
              '            <div class="form-group">\n' +
              '                <label for="string2">String 2</label>\n' +
              '                <input type="text" class="form-control" name="string2[]">\n' +
              '            </div>\n' +
              '            <div class="form-group">\n' +
              '                <label for="i">C value</label>\n' +
              '                <input type="text" class="form-control" name="c[]">\n' +
              '            </div>\n' +
              '            <div class="form-group">\n' +
              '                <label for="i">I value</label>\n' +
              '                <input type="text" class="form-control" name="i[]">\n' +
              '            </div>')
        }
      }
    });

  });
})(jQuery);
