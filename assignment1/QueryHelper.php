<?php

class QueryHelper {
  private $dlength;
  private $wlength;
  private $darray;
  private $qlength;
  private $qarray;
  private $carray;

  public function __construct ($dlength, $darray, $wlength, $qlength, $qarray) {
    $this->dlength = $dlength;
    $this->darray = $darray;
    $this->wlength = $wlength;
    $this->qlength = $qlength;
    $this->qarray = $qarray;
    $this->carray = array();
  }

  private function generateOutput() {
    for($i=0; $i < $this->qlength; $i++) {
      $query_pattern = $this->generatePattern($this->qarray[$i]);
      $this->carray[$i] = $this->searchDictionary($query_pattern);
    }
  }

  private function searchDictionary($query_pattern) {
    $matches = preg_grep($query_pattern, $this->darray);
    return count($matches);
  }

  private function generatePattern($query_string) {
    if(empty($query_string)) {
      return;
    }
    $pattern = '/^';
    $conlength = 0;
    for($i=0; $i < strlen($query_string); $i++) {
      if($query_string[$i] == '?') {
        if($conlength === 0) {
          $pattern.='[a-z]';
        }
        $conlength++;
        if( $i == (strlen($query_string)-1)){
          $pattern.= '{'.$conlength.'}';
        }
      }else{
        if($conlength) {
          $pattern.= '{'.$conlength.'}';
        }
        $pattern.=$query_string[$i];
        $conlength = 0;
      }
    }
    return $pattern.'$/';
  }

  public function renderOutput(){
    $this->generateOutput();
    return $this->carray;
  }
}
